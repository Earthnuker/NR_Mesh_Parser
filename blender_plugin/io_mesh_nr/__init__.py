import os
import sys

import bmesh
import bpy
from . import nr_obj
from bpy.props import BoolProperty, EnumProperty, StringProperty
from bpy.types import Operator
from bpy_extras.io_utils import ImportHelper
from mathutils import Vector

bl_info = {
    'name': 'Import Nitronic Rush model',
    'author': 'Daniel Seiller',
    'version': (0, 0, 6),
    'location': 'File > Import > Import Nitronic Rush Model',
    'description': 'Import xml Objects from the Game Nitronic Rush',
    "wiki_url": "https://gitlab.com/Earthnuker/NR_Mesh_Parser/wikis/home",
    "tracker_url": "https://gitlab.com/Earthnuker/NR_Mesh_Parser/issues",
    'category': 'Import-Export'
}
__version__ = '.'.join([str(s) for s in bl_info['version']])


def load_model(context, filepath, load_material, scale_model):
    print("running load_model...")
    game_root = os.path.abspath(os.path.join(
        os.path.dirname(filepath), os.path.pardir))
    if not os.path.isfile(os.path.join(game_root, "NitronicRush.exe")):
        raise RuntimeError("NitronicRush.exe not found, bailing out!")
    nr_obj.game_root = game_root
    nitr_obj = nr_obj.NitronicObject(filepath)
    nitr_msh = nitr_obj.object.get('Mesh', None)
    if nitr_msh is None:
        raise RuntimeError("Object file does not contain a Mesh")
    nitr_mat = nitr_obj.object.get('Materials', None)
    verts, uvs, normals = zip(
        *[(vert.position, vert.uv, vert.normal) for vert in nitr_msh.vtxs])
    meshName = os.path.basename(nitr_obj.object['Mesh'].file_name)

    me = bpy.data.meshes.new(meshName)
    me.from_pydata(verts, [], nitr_msh.tris)
    me.update(calc_edges=True)

    obName = os.path.basename(filepath)
    ob = bpy.data.objects.new(obName, me)
    scn = bpy.context.scene
    scn.objects.link(ob)

    for idx, vert in enumerate(me.vertices):
        me.vertices[idx].normal = Vector(normals[idx][:3])
    bpy.context.scene.render.engine = 'CYCLES'
    ob.select = True
    scn.objects.active = ob
    if (not load_material) or (nitr_mat is None):
        return {'FINISHED'}
    # load uv coords
    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(me)
    uv_layer = bm.loops.layers.uv.verify() or bm.loops.layers.uv.new()
    bm.faces.layers.tex.verify()
    for f in bm.faces:
        for l in f.loops:
            luv = l[uv_layer]
            luv.uv = tuple(uvs[l.vert.index])
    bmesh.update_edit_mesh(me)
    if scale_model:
        scale_factor = 1.0 / nitr_msh.default_scale
        bpy.ops.transform.resize(
            value=(scale_factor, scale_factor, scale_factor))
    # bpy.ops.transform.translate(value=b_box_center)
    bpy.ops.object.mode_set(mode='OBJECT')
    # setup materials

    mat = bpy.data.materials.new(name=os.path.splitext(obName)[0])
    ob.active_material = mat
    mat.use_nodes = True
    node_tree = mat.node_tree
    nodes = node_tree.nodes
    # remove default diffuse
    for n in nodes:
        nodes.remove(n)
    shader_nodes = {
        'mat_out': "ShaderNodeOutputMaterial",
        'emit': "ShaderNodeEmission",
        'tex_co': "ShaderNodeTexCoord",
        'bump_map': "ShaderNodeNormalMap",
        'emit_hue': "ShaderNodeMixRGB",
        'tex_emit': "ShaderNodeTexImage",
        'tex_diff': "ShaderNodeTexImage",
        'tex_norm': "ShaderNodeTexImage",
        'emit_mult': "ShaderNodeMath",
        'fresnel': "ShaderNodeFresnel",
        'glossy': "ShaderNodeBsdfGlossy",
        'diffuse': "ShaderNodeBsdfDiffuse",
        'mix': "ShaderNodeMixShader",
        'add': "ShaderNodeAddShader",
        'gamma': "ShaderNodeGamma",
    }
    for k, node in shader_nodes.copy().items():
        shader_nodes[k] = nodes.new(node)
    connections = [
        [
            ('tex_co', 2),
            [('tex_diff', 0), ('tex_emit', 0), ('tex_norm', 0)]
        ],
        [
            ('tex_diff', 0),
            [('gamma', 0)]
        ],
        [
            ('gamma', 0),
            [('glossy', 0), ('diffuse', 0)]
        ],
        [
            ('fresnel', 0),
            [('mix', 0)]
        ],
        [
            ('glossy', 0),
            [('mix', 1)]
        ],
        [
            ('diffuse', 0),
            [('mix', 2)]
        ],
        [
            ('mix', 0),
            [('add', 0)]
        ],
        [
            ('tex_norm', 0),
            [('bump_map', 1)]
        ],
        [
            ('tex_norm', 1),
            [('bump_map', 0)]
        ],
        [
            ('bump_map', 0),
            [('glossy', 2), ('diffuse', 2), ('fresnel', 1)]
        ],
        [
            ('tex_emit', 0),
            [('emit_hue', 1)]
        ],
        [
            ('tex_emit', 1),
            [('emit_mult', 0)]
        ],

        [
            ('emit_hue', 0),
            [('emit', 0)]
        ],
        [
            ('emit_mult', 0),
            [('emit', 1)]
        ],
        [
            ('emit', 0),
            [('add', 1)]
        ],
        [
            ('add', 0),
            [('mat_out', 0)]
        ],
    ]
    defaults = [
        ('emit_hue', 0, 1.0),
        #('emit_hue', 2, (1.0, 0.25, 0.0, 1.0)), # orange
        ('emit_hue', 2, (1.0, 1.0, 1.0, 1.0)),
        ('emit_mult', 1, 15.0),
        ('gamma', 1, 0.5),
        ('fresnel', 0, 1.45),
    ]
    attrs = [
        ('emit_mult', 'operation', 'MULTIPLY'),
        ('emit_hue', 'blend_type', 'HUE'),
        ('bump_map', 'uv_map', me.uv_layers.keys()[0]),
        ('tex_norm', 'color_space', 'NONE'),
        ('glossy', 'distribution', 'SHARP'),
    ]

    for (node_src, sock_src), dsts in connections:
        print(node_src, sock_src, dsts)
        conn_from = shader_nodes[node_src].outputs[sock_src]
        for node_dst, sock_dst in dsts:
            print(node_dst, sock_dst)
            conn_to = shader_nodes[node_dst].inputs[sock_dst]
            node_tree.links.new(conn_from, conn_to)

    for node, attr, value in attrs:
        setattr(shader_nodes[node], attr, value)

    for node, inp, value in defaults:
        print(node, inp, value)
        shader_nodes[node].inputs[inp].default_value = value

    for tex_type, tex_file in nitr_mat.tex.items():
        tex_file = os.path.join(game_root, "Textures", tex_file)
        img = bpy.data.images.load(tex_file)
        if tex_type.startswith("Emit"):
            shader_nodes['tex_emit'].image = img
        if tex_type.startswith("Normal"):
            shader_nodes['tex_norm'].image = img
        if tex_type.startswith("Diffuse"):
            shader_nodes['tex_diff'].image = img

    return {'FINISHED'}


class ImportNRModel(Operator, ImportHelper):
    """Import Nitronic Rush Model from XML file"""
    bl_idname = "import_scene.nitronic_rush"
    bl_label = "Import Model"

    filename_ext = ".xml"

    filter_glob = StringProperty(
        default="*.xml",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )
    load_material = BoolProperty(
        name="Load Material",
        description="Import Textures and Load Materials",
        default=True,
    )
    scale_model = BoolProperty(
        name="Normalize Scale",
        description="Normalize Scale according to data stored in mesh",
        default=True,
    )
    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.

    def execute(self, context):
        return load_model(context, self.filepath, self.load_material, self.scale_model)


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(ImportNRModel.bl_idname,
                         text="Import Nitronic Rush Model")


def register():
    bpy.utils.register_class(ImportNRModel)
    bpy.types.INFO_MT_file_import.append(menu_func_import)


def unregister():
    bpy.utils.unregister_class(ImportNRModel)
    bpy.types.INFO_MT_file_import.remove(menu_func_import)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.import_scene.nitronic_rush('INVOKE_DEFAULT')
