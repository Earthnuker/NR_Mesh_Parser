import io
import math
import os
import shutil
import struct
import sys
import xml.etree.ElementTree as ET


def strip_num(s):
    n = ""
    while s[-1].isnumeric():
        n = s[-1] + n
        s = s[:-1]
    if n:
        n = int(n)
    else:
        n = None
    return n, s


class NitronicObject(object):

    def __init__(self, file_name):
        print("Loading {}".format(file_name))
        tree = ET.parse(file_name)
        root = tree.getroot()
        nop = lambda x: x
        type_map = {
            'String': str,
            'Integer': int,
            'Float': float
        }
        self.object = {}
        for RM in root.iter("RenderMesh"):
            for prop in RM.getchildren():
                tag = prop.tag
                name = prop.attrib.get('name', None)
                value = (prop.text or prop.attrib.get('value', None)).strip()
                if tag.startswith("Vector"):
                    value = [type_map.get(child.tag, nop)(
                        child.attrib['value']) for child in prop.getchildren()]
                value = type_map.get(tag, nop)(value)
                self.object[name] = value
                print("{}: {}".format(name, value))
            self.load_mesh()
            self.load_materials()

    def load_materials(self):
        self.object['Materials'] = NitronicMat()
        for mat in range(self.object['Material Count']):
            mat_name = "Material{}".format(mat + 1)
            mat_file_name = self.object.get(mat_name, None)
            if mat_file_name is None:
                continue
            mat_path = os.path.join(
                game_root, "Materials", mat_file_name + '.txt')
            try:
                self.object['Materials'].update(mat_path)
            except (FileNotFoundError) as e:
                print(str(e))
        print("Textures:")
        for tn, tf in self.object['Materials'].tex.items():
            print("{}:".format(tn), tf)
        print("Shaders:")
        for fn, ff in self.object['Materials'].fx.items():
            print("{}:".format(fn), ff)

    def load_mesh(self):
        try:
            self.object['Mesh'] = NitronicMesh(os.path.join(
                game_root, "Meshes", self.object['Mesh']))
        except (AssertionError, FileNotFoundError) as e:
            del self.object['Mesh']
            print(str(e))

    def to_obj(self):
        if 'Mesh' in self.object and 'Materials' in self.object:
            return OBJ(self.object['Mesh'], self.object['Materials'])

    def __getitem__(self, item):
        return self.object[item]


class NitronicMat(object):

    def __init__(self, mat_path=None):
        self.fx = {}
        self.tex = {}
        if mat_path is not None:
            self.update(mat_path)

    def update(self, material_path):
        print("Loading", material_path)
        with open(material_path) as infile:
            for line in infile:
                line = line.strip()
                if not line or line[0] == "#":
                    continue
                if line.startswith("PARAM TEXTURE"):
                    args = line.split(" ")[2:]
                    tex_type, tex = args
                    self.tex[tex_type] = tex
                if line.startswith("PASS"):
                    args = line.split(" ")[1:]
                    fx_type, fx = args
                    self.fx[fx_type] = fx
                if line.startswith("LOAD MATERIAL"):
                    args = " ".join(line.split(" ")[2:])
                    path = os.path.join(game_root, "Materials", args + '.txt')
                    res = NitronicMat(path)
                    self.fx.update(res.fx)
                    self.tex.update(res.tex)


class NitronicMesh(object):

    def __init__(self, file_name):
        self.file_name = file_name
        self.fh = open(self.file_name, 'rb')
        self.parse_header()
        self.read_mesh_data()

    def parse_header(self):
        """Read Model Header"""
        print("Loading", self.file_name)
        self.file_type = int.from_bytes(self.fh.read(4), 'little')
        assert self.file_type == 0x6, "Invalid file type"
        print("File Type:", self.file_type)
        self.vtx_fmt = str(bytes(iter(lambda: self.fh.read(1)[0], 0)), 'utf-8')
        print("Vertex Data Format:", self.vtx_fmt)
        self.submeshe_cnt = int.from_bytes(self.fh.read(4), 'little')
        print("Submeshes:", self.submeshe_cnt)
        self.submeshes = [int.from_bytes(self.fh.read(
            4), 'little') for _ in range(self.submeshe_cnt)]
        print("Submesh Indices:", self.submeshes)
        self.tri_cnt = int.from_bytes(self.fh.read(4), 'little')
        assert self.tri_cnt % 3 == 0, "Invalid Tri Count"
        self.tri_cnt = self.tri_cnt // 3
        print("Tris:", self.tri_cnt)
        self.vtx_cnt = int.from_bytes(self.fh.read(4), 'little')
        print("Verts:", self.vtx_cnt)

    def make_struct(self):
        """Prepare Vertex Data Struct"""
        buffer = io.StringIO(self.vtx_fmt)
        self.structs = []
        while True:
            v = buffer.read(1)
            if not v:
                break
            n = buffer.read(1)
            if n.isnumeric():
                self.structs.append((v, "<{}f".format(n)))
            else:
                self.structs.append((v + n, "<{}B".format(2 * len(v + n))))

    def get_vtx(self):
        """Read Vertex Data"""
        keys = [key for key, fmt in self.structs]
        key_to_name = {'p': 'position', 'cc': 'color',
                       't': 'tangent', 'u': 'uv', 'n': 'normal', 'b': 'bitangent'}
        vtx = {}
        for key, fmt in self.structs:
            size = struct.calcsize(fmt)
            vtx[key_to_name.get(key, key)] = struct.unpack(
                fmt, self.fh.read(size))
        vtx = type("Vertex", (object,), vtx)
        # flip V(Y) coordinate
        vtx.uv = (vtx.uv[0], 1 - vtx.uv[1])
        return vtx

    def read_mesh_data(self):
        print("Reading Tris")
        self.tris = []
        for idx in range(self.tri_cnt):
            tri = [int.from_bytes(self.fh.read(2), 'little') for _ in range(3)]
            self.tris.append(tri)
        print("Reading Verts")
        self.make_struct()
        self.vtxs = []
        smn = 0
        for idx in range(self.vtx_cnt):
            if idx in self.submeshes:
                print("Start of Submesh", smn, "at", idx)
                smn += 1
            vtx = self.get_vtx()
            self.vtxs.append(vtx)
        self.bounding_box = []
        self.bounding_box.append(struct.unpack("<3f", self.fh.read(4 * 3)))
        self.bounding_box.append(struct.unpack("<3f", self.fh.read(4 * 3)))
        # swap x1 and x2
        self.bounding_box[0][0], self.bounding_box[1][
            0] = self.bounding_box[1][0], self.bounding_box[0][0]
        self.default_scale = struct.unpack("<f", self.fh.read(4))[0]
        assert len(self.fh.read()
                   ) == 0, "We have Data left over, something went wrong!"


class OBJ(object):
    verts = []
    faces = []
    normals = []
    uvs = []

    def __init__(self, mesh, material):
        self.mesh = mesh
        self.material = material
        self.name = os.path.basename(self.mesh.file_name)

    def export_mat(self):
        s = "newmtl {}\n".format(self.name)
        for tex_name, tex in self.material.tex.items():
            shutil.copy(os.path.join(game_root, "Textures", tex), ".")
            if tex_name.startswith("Diffuse"):
                s += "map_Kd {}\n".format(tex)
            if tex_name.startswith("Specular"):
                s += "map_Ks {}\n".format(tex)
            if tex_name.startswith("Normal"):
                s += "disp {}\n".format(tex)
            if tex_name.startswith("Emit"):
                s += "map_Ka {}\n".format(tex)
        return s

    def export_mesh(self):
        for a, b, c in self.mesh.tris:
            a += 1
            b += 1
            c += 1
            self.faces.append((a, b, c))
        for vert in self.mesh.vtxs:
            self.verts.append(vert.position)
            self.normals.append(vert.normal)
            self.uvs.append(vert.uv)
        s = "o {}\n".format(self.name)
        s += "mtllib {}\n".format(self.name + ".mtl")
        for vert in self.verts:
            s += "v {} {} {}\n".format(*vert)
        for normal in self.normals:
            s += "vn " + " ".join(map(str, normal)) + "\n"
        for uv in self.uvs:
            s += "vt " + " ".join(map(str, uv)) + "\n"
        s += "usemtl {}\n".format(self.name)
        for face in self.faces:
            face = map(lambda f: "{}/{}/{}".format(f, f, f), face)
            s += "f " + " ".join(face) + "\n"
        return s
