Blender Plugin for importing Nitronic Rush Models into Blender

Installation:

just copy the io_mesh_nr folder to your Blender addons/scripts folder:

Windows: **C:&#92;Users&#92;[username]&#92;AppData&#92;Roaming&#92;Blender Foundation&#92;Blender&#92;[version]&#92;scripts&#92;addons&#92;**

Linux: **/home/[username]/.blender/[version]/scripts/addons/**

Mac OSX: **Macintosh HD/Users/[username]/Library/Application Support/Blender/[version]/scripts/addons/**

Demo GIF:

![demo](demo.gif)
