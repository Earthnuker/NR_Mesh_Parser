import os
import sys

from construct import *


class FacesValidator(Validator):

    def _validate(self, obj, ctx):
        valid = all([idx < ctx.num_vtx for idx in obj])
        return valid

Vertex = Struct("verts",
                Array(3, LFloat32('pos')),
                Array(4, LFloat32('norm')),
                Array(4, ULInt8('col')),
                Array(2, LFloat32('uv')),
                Array(3, LFloat32('tan')),
                Array(3, LFloat32('btan')),
                )

BoundingBox = Struct("bounding_box",
                     LFloat32('x2'),
                     LFloat32('y1'),
                     LFloat32('z1'),
                     LFloat32('x1'),
                     LFloat32('y2'),
                     LFloat32('z2'),
                     )

Mesh = Struct("Mesh",
              Const(ULInt32('identifier'), 6),
              Const(CString('format_descriptor', encoding='utf-8'), 'p3n4ccu2t3b3'),
              ULInt32('submesh_count'),
              Array(lambda ctx: ctx.submesh_count,
                    ULInt32('submesh_idx')),
              ULInt32('num_tris'),
              ULInt32('num_vtx'),
              Array(lambda ctx: ctx.num_tris // 3,
                    FacesValidator(Array(3, ULInt16('faces')))
                    ),
              Array(lambda ctx: ctx.num_vtx,
                    Vertex
                    ),
              BoundingBox,
              LFloat32('default_scale'),
              Terminator
              )

if not len(sys.argv) == 2:
    print("Usage: {} <Nitronic Rush Installation Folder>".format(
        os.path.split(sys.argv[0])[-1]))
    exit(-1)

for root, dirs, files in os.walk(os.path.join(sys.argv[1], "Meshes")):
    total_files = 0
    ok_files = 0
    for filename in files:
        if not filename.endswith(".msh"):
            continue
        total_files += 1
        file_path = os.path.join(root, filename)
        with open(file_path, 'rb') as infile:
            data = infile.read()
        print(filename, flush=True, end=" ")
        try:
            mesh = Mesh.parse(data)
            rebuilt = Mesh.build(mesh)
            assert data == rebuilt
            print("OK!", flush=True)
            ok_files += 1
        except (AssertionError, ConstError) as e:
            print("ERROR: ", str(e))
print("{} of {} files parsed and rebuilt sucessfully!".format(ok_files, total_files))
