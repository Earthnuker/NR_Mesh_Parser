Parser and Converter for Nitronic Rush Mesh Format

Mesh Format Reverse Engineered by Ciastex (<https://github.com/Ciastex/>)

[Ciastex's Original Mesh Format Documetation](https://onedrive.live.com/redir?resid=A18D02C9428A323E!85338&authkey=!ANBGTFXqFIGMvtE&ithint=file%2cdocx)

Blender Plugin for importing meshes can be found in the blender_plugin subdirectory

Mesh Format as C++ struct is:

```c++
struct Mesh {
  struct Header {
    uint32_t identifier; // 0x6
    char format_descriptor[13];
    uint32_t submesh_count;
    uint32_t submesh_indices[submesh_count];
    uint32_t num_tris;
    uint32_t num_vertex;
  };
  struct Faces {
    uint32_t vertex_idx[3];
  } tris[num_tris/3];
  struct Vertex {
    float pos[3];
    float normal[4];
    uint8_t color[4];
    float uv[2];
    float tangent[3];
    float bitangent[3];
  } vertices[num_vertex];
  float bounding_box[2*3]; // X1 Y1 Z1 X2 Y2 Z2
  float default_scale;
}
```
